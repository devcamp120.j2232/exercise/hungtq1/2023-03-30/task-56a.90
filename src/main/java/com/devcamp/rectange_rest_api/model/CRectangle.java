package com.devcamp.rectange_rest_api.model;

public class CRectangle {
     
    private float length = 1f;
    private float width = 1f;
   
    public CRectangle() {
    }
    
    public CRectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
    
    public float getLength() {
        return length;
    }
    
    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }
   
    public double getArena(){
        return this.width * this.length;
    }

    
    public double getPerimeter(){
        return (this.width + this.length) * 2;
    }
   
    public String toString(){
        return "Rectangle[ length = " + this.length + ", width = " + this.width + "]";
    }   

    }

